/***********************************************************
// OOP244 Final Project Milestone 2
// ErrorState.h
// Date: 2018/03/22
// Author: Yiran Zhu
// Email: yzhu132@myseneca.ca
// Description: This milestone focuses on the ErrorState class
***********************************************************/


#ifndef AMA_ERRORSTATE_H
#define AMA_ERRORSTATE_H
#include <iostream>
#include <cstring>

namespace AMA {
	class ErrorState {
		char* storedMessage;
	public:
		explicit ErrorState(const char* errorMessage = nullptr);
		ErrorState(const ErrorState& em) = delete;
		ErrorState& operator=(const ErrorState& em) = delete;
		virtual ~ErrorState();
		void clear();
		bool isClear() const;
		void message(const char* str);
		const char* message() const;
	};
	std::ostream& operator<<(std::ostream& os, const ErrorState& es);
}

#endif