/***********************************************************
// OOP244 Final Project Milestone 5
// Perishable.h
// Date: 2018/04/09
// Author: Yiran Zhu
// Email: yzhu132@myseneca.ca
// Description: This milestone focuses on the Perishable class
***********************************************************/

#include "Perishable.h"

namespace AMA {
	Perishable::Perishable() : Product('P') {
		expiryDate = Date();
	}

	std::fstream& Perishable::store(std::fstream& file, bool newLine) const {
		Product::store(file, false);
		file << "," << expiryDate;
		if (newLine)
			file << "\n";
		return file;
	}

	std::fstream& Perishable::load(std::fstream& file) {
		char tempChar;
		Product::load(file);
		file >> expiryDate;
		file >> tempChar;
		return file;
	}

	std::ostream& Perishable::write(std::ostream& os, bool linear) const {
			Product::write(os, linear);
			if (isClear() && !isEmpty()) {
				if (linear) {
					os << expiryDate;
				}
				else {
					os << " Expiry date: " << expiryDate;
				}
			}
		return os;
	}

	std::istream& Perishable::read(std::istream& is) {
		Product::read(is);
		if (!isEmpty()) {
			std::cout << " Expiry date (YYYY/MM/DD): ";
			Date tempDate;
			is >> tempDate;
			if (tempDate.bad()) {
				if (tempDate.errCode() == CIN_FAILED) {
					message("Invalid Date Entry");
				} else if (tempDate.errCode() == YEAR_ERROR) {
					message("Invalid Year in Date Entry");
				} else if (tempDate.errCode() == MON_ERROR) {
					message("Invalid Month in Date Entry");
				} else if (tempDate.errCode() == DAY_ERROR) {
					message("Invalid Day in Date Entry");
				}
				is.setstate(std::ios::failbit);
			}
			if (!is.fail()) {
				expiryDate = tempDate;
			}
		}
		return is;
	}

	const Date& Perishable::expiry() const {
		return expiryDate;
	}
}
