/***********************************************************
// OOP244 Final Project Milestone 3
// Product.cpp
// Date: 2018/04/09
// Author: Yiran Zhu
// Email: yzhu132@myseneca.ca
// Description: This milestone focuses on the Product class
***********************************************************/

#include "Product.h"
#include <iomanip>

namespace AMA {

	void Product::name(const char* given_name) {
		if (given_name != nullptr) {
			SKUName = new char[max_name_length + 1];
			strncpy(SKUName, given_name, max_name_length);
		} else {
			SKUName = nullptr;
		}
	}

	const char* Product::name() const {
		if (SKUName != nullptr)
			return SKUName;
		return nullptr;
	}

	const char* Product::sku() const {
		return SKU;
	}

	const char* Product::unit() const { 
		return SKUDesc;
	}

	bool Product::taxed() const {
		return taxStatus;
	}

	double Product::price() const {
		return productPrice;
	}

	double Product::cost() const {
		if (taxStatus) {
			return productPrice*(1 + static_cast<double>(Tax_Rate/100.00));
		}
		return price();
	}

	void Product::message(const char* error_message){
		errorState.message(error_message);
	}

	bool Product::isClear() const {
		return errorState.isClear();
	}

	Product::Product(char given_type) {
		type = given_type;
		// safe state
		quantityOnHand = 0;
		quantityNeeded = 0;
	}

	Product::Product(const char* sku, const char* given_name, const char* unit, int qtyOnHand, bool status, double price, int qtyNeeded) {
		strncpy(SKU, sku, max_sku_length);
		name(given_name);
		strncpy(SKUDesc, unit, max_unit_length);
		quantity(qtyOnHand);
		taxStatus = status;
		productPrice = price;
		quantityNeeded = qtyNeeded;
	}

	Product::Product(const Product& copy) {
		*this = Product(copy.sku(), copy.name(), copy.unit(), copy.quantity(), copy.taxed(), copy.price(), copy.qtyNeeded());
	}

	Product& Product::operator=(const Product& rhs) {
		strncpy(SKU, rhs.sku(), max_sku_length);
		name(rhs.name());
		strncpy(SKUDesc, rhs.unit(), max_unit_length);
		quantityOnHand = rhs.quantity();
		taxStatus = rhs.taxed();
		productPrice = rhs.price();
		quantityNeeded = rhs.qtyNeeded();
		return *this;
	}

	Product::~Product() {
		delete[] SKUName;
	}

	std::fstream& Product::store(std::fstream& file, bool addNewLine) const {
		file << type << ',' << SKU << ',' << SKUName << ',' << SKUDesc << ',' << taxStatus << ',' << productPrice << ',' << quantityOnHand << ',' << quantityNeeded;
		if (addNewLine)
			file << "\n";
		return file;
	}

	std::fstream& Product::load(std::fstream& file) {
		char tempSku[max_sku_length + 1];
		char tempSKUName[max_name_length + 1];
		char tempSKUDesc[max_unit_length + 1];
		char tempTaxString[5];
		bool tempTaxStatus;
		char tempPriceArray[100];
		double tempProductPrice;
		char tempQuantityString[20];
		int tempQuantityOnHand;
		char tempQtyNeedString[20];
		int tempQuantityNeeded;

		file.getline(tempSku, max_sku_length, ',');
		file.getline(tempSKUName, max_name_length, ',');
		file.getline(tempSKUDesc, max_unit_length, ',');
		file.getline(tempTaxString, 20, ',');
		if (tempTaxString[0] == '0') {
			tempTaxStatus = false;
		}
		else {
			tempTaxStatus = true;
		}
		file.getline(tempPriceArray, 1000, ',');
		tempProductPrice = atof(tempPriceArray);
		file.getline(tempQuantityString, 1000, ',');
		tempQuantityOnHand = atoi(tempQuantityString);
		file.getline(tempQtyNeedString, 1000, ',');
		tempQuantityNeeded = atoi(tempQtyNeedString);
		*this = Product(tempSku, tempSKUName, tempSKUDesc, tempQuantityOnHand, tempTaxStatus, tempProductPrice, tempQuantityNeeded);
		return file;
	}

	std::ostream& Product::write(std::ostream& os, bool linear) const {
		if (!isClear()) {
			os << errorState.message();
		}
		else {
			if (linear) {
				std::cout.width(max_sku_length);
				os << std::left << SKU << "|"; 
				std::cout.width(20);
				os << SKUName << "|";
				std::cout.width(7);
				os.setf(std::ios::fixed);
				os.precision(2);
				os << std::right << cost() << "|";
				std::cout.width(4);
				os << quantityOnHand << "|";
				std::cout.width(10);
				os << std::left << SKUDesc << "|";
				std::cout.width(4);
				os << std::right << quantityNeeded << "|";
			} else {
				os << " Sku: " << SKU << std::endl;
				os << " Name (no spaces): " << SKUName << std::endl;
				os << " Price: " << productPrice << std::endl;
				os << " Price after tax: ";
				if (taxStatus)
					os << cost() << std::endl;
				else
					os << "N/A" << std::endl;
				os << " Quantity on Hand: " << quantity() << " " << unit() << std::endl;
				os << " Quantity needed: " << qtyNeeded();
				std::cin.ignore();
			}
		}
		return os;
	}

	std::istream& Product::read(std::istream& is) {
		char tempSku[max_sku_length + 1];
		char tempSKUName[max_name_length + 1];
		char tempSKUDesc[max_unit_length + 1];
		char tempTax;
		bool tempTaxStatus;
		double tempPrice;
		int tempQuantityOnHand;
		int tempQuantityNeeded;
		
		errorState.clear();
		std::cout << " Sku: ";
		is >> tempSku; 
		std::cout << " Name (no spaces): ";
		is >> tempSKUName; 
		std::cout << " Unit: ";
		is >> tempSKUDesc;
		std::cout << " Taxed? (y/n): ";
		is >> tempTax;
		if (toupper(tempTax) != 'Y' && toupper(tempTax) != 'N') {
			message("Only (Y)es or (N)o are acceptable");
			is.setstate(std::ios::failbit);
		} else {
			if (toupper(tempTax) == 'Y') {
				tempTaxStatus = true;
			} else if (toupper(tempTax) == 'N') {
				tempTaxStatus = false;
			}
		}
		if(isClear()){
			std::cout << " Price: ";
			is >> tempPrice;
			if (is.fail()) {
				message("Invalid Price Entry");
			}
		}
		if (isClear()) {
			std::cout << " Quantity on hand: ";
			is >> tempQuantityOnHand;
			if (is.fail()) {
				message("Invalid Quantity Entry");
			}
		}
		if (isClear()) {
			std::cout << " Quantity needed: ";
			is >> tempQuantityNeeded;
			if (is.fail()) {
				message("Invalid Quantity Needed Entry");
			}
		}
		if (isClear()) {
			*this = Product(tempSku, tempSKUName, tempSKUDesc, tempQuantityOnHand, tempTaxStatus, tempPrice, tempQuantityNeeded);
		}
		return is;
	}

	bool Product::operator==(const char* sku) const {
		if (std::strcmp(this->SKU, sku) == 0) {
			return true;
		}
		return false;
	}

	double Product::total_cost() const {
		if (taxStatus) {
			return (quantityOnHand * productPrice * (1 + static_cast<double>(Tax_Rate / 100.00)));
		}
		return (quantityOnHand * productPrice);
	}

	void Product::quantity(int unit) {
		quantityOnHand = unit;
	}

	bool Product::isEmpty() const {
		return (quantityOnHand == 0) && (quantityNeeded == 0);
	}

	int Product::qtyNeeded() const {
		return quantityNeeded;
	}

	int Product::quantity() const {
		return quantityOnHand;
	}

	bool Product::operator>(const char* name) const {
		if (std::strcmp(this->SKUName, name) > 0) {
			return true;
		}
		return false;
	}

	bool Product::operator>(const iProduct& p) const {
		if (std::strcmp(this->SKUName, p.name()) > 0) {
			return true;
		}
		return false;
	}

	int Product::operator+=(int unit) {
		if (unit > 0) {
			quantityOnHand += unit;
		}
		return quantityOnHand;
	}

	std::ostream& operator<<(std::ostream& os, const iProduct& p)
	{
		p.write(os, true);
		return os;
	}

	std::istream& operator>>(std::istream& is, iProduct& p)
	{
		p.read(is);
		return is;
	}

	double operator+=(double& cost, const iProduct& p)
	{
		cost += p.total_cost();
		return cost;
	}

}