/***********************************************************
// OOP244 Final Project Milestone 1
// Date.cpp
// Date: 2018/03/16
// Author: Yiran Zhu
// Email: yzhu132@myseneca.ca
// Description: This milestone focuses on the Date class
***********************************************************/

#include "Date.h"
#include <string>


namespace AMA {

	//Private methods
	void Date::errCode(int errorCode) {
		this->error = errorCode;
	}

	// number of days in month mon_ and year year_
	int Date::mdays(int mon, int year)const {
		int days[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31, -1 };
		int month = mon >= 1 && mon <= 12 ? mon : 13;
		month--;
		return days[month] + int((month == 1)*((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0));
	}

	//Public methods
	Date::Date() {
		year = 0;
		month = 0;
		day = 0;
		comparator = 0;
		errCode(NO_ERROR);
	}

	Date::Date(int givenYear, int givenMonth, int givenDay) {
		if (givenYear < min_year || givenYear > max_year) {
			errCode(YEAR_ERROR);
		} else if (givenMonth < 1 || givenMonth > 12) {
			errCode(MON_ERROR);
		} else if (givenDay < 1 || givenDay > mdays(givenMonth, givenYear)) {
			errCode(DAY_ERROR);
		} else {
			errCode(NO_ERROR);
		}

		if (error == NO_ERROR) {
			year = givenYear;
			month = givenMonth;
			day = givenDay;
			comparator = year * 372 + month * 13 + day;
		} else {
			year = 0;
			month = 0;
			day = 0;
			comparator = 0;
		}

	}

	bool Date::operator==(const Date& rhs) const {
		if (this->comparator == 0 || rhs.comparator == 0)
			return false;
		if (this->comparator == rhs.comparator) {
			return true;
		} else {
			return false;
		}
	}

	bool Date::operator!=(const Date& rhs) const {
		if (this->comparator == 0 || rhs.comparator == 0)
			return false;
		return !(*this==rhs);
	}

	bool Date::operator<(const Date& rhs) const {
		if (this->comparator == 0 || rhs.comparator == 0)
			return false;
		if (this->comparator < rhs.comparator) {
			return true;
		} else {
			return false;
		}
	}

	bool Date::operator>(const Date& rhs) const {
		if (this->comparator == 0 || rhs.comparator == 0)
			return false;
		if (this->comparator > rhs.comparator) {
			return true;
		}
		else {
			return false;
		}
	}

	bool Date::operator<=(const Date& rhs) const {
		if (this->comparator == 0 || rhs.comparator == 0)
			return false;
		return !(*this > rhs);
	}

	bool Date::operator>=(const Date& rhs) const {
		if (this->comparator == 0 || rhs.comparator == 0)
			return false;
		return !(*this < rhs);
	}

	int Date::errCode() const {
		return this->error;
	}

	bool Date::bad() const {
		return (errCode() != NO_ERROR);
	}

	std::istream& Date::read(std::istream& istr) {
		char delimiter;
		int tempYear, tempMonth, tempDay;

		istr >> tempYear;
		istr >> delimiter;
		istr >> tempMonth;
		istr >> delimiter;
		istr >> tempDay;

		if (istr.fail() || (delimiter != '/' && delimiter != '-')) {
			errCode(CIN_FAILED);
			return istr;
		}

		if (!istr.fail()) {
			if (tempYear < min_year || tempYear > max_year) {
				errCode(YEAR_ERROR);
				return istr;
			}
			else if (tempMonth < 1 || tempMonth > 12) {
				errCode(MON_ERROR);
				return istr;
			}
			else if (tempDay < 1 || tempDay > mdays(tempMonth, tempYear)) {
				errCode(DAY_ERROR);
				return istr;
			}
		}
		if (!this->bad()) {
			*this = Date(tempYear, tempMonth, tempDay);
		}
		return istr;
	}

	std::ostream& Date::write(std::ostream& ostr) const {
		if (errCode() == NO_ERROR) {
			ostr << this->year << "/";
			if (this->month < 10)
				ostr << "0";
			ostr << this->month << "/";
			if (this->day < 10)
				ostr << "0";
			ostr << this->day;
		}
		else {
			ostr << "0/00/00";
		}
		return ostr;
	}
	
	std::ostream& operator<<(std::ostream& os, const Date& d) {
		d.write(os);
		return os;
	}

	std::istream& operator>>(std::istream& is, Date& d) {
		d.read(is);
		return is;
	}
}
