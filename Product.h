/***********************************************************
// OOP244 Final Project Milestone 3
// Product.h
// Date: 2018/04/09
// Author: Yiran Zhu
// Email: yzhu132@myseneca.ca
// Description: This milestone focuses on the Product class
***********************************************************/

#ifndef AMA_PRODUCT_H
#define AMA_PRODUCT_H

#include "ErrorState.h"
#include "iProduct.h"
#include <iostream>
#include <fstream>
#include <cstring>

namespace AMA {

	#define max_sku_length  7
	#define	max_name_length 10
	#define	max_unit_length 75
	#define	Tax_Rate		13

	class Product : public iProduct {
		char type;
		char SKU[max_sku_length + 1];
		char SKUDesc[max_unit_length + 1];
		char* SKUName;
		int quantityOnHand;
		int quantityNeeded;
		double productPrice;
		bool taxStatus;
		ErrorState errorState;
	protected:
		void name(const char*);
		const char* name() const;
		const char* sku() const;
		const char* unit() const;
		bool taxed() const;
		double price() const;
		double cost() const;
		void message(const char*);
		bool isClear() const;
	public:
		Product(char type = 'N');
		Product(const char* sku, const char* name, const char* unit, int qtyOnHand = 0, bool status = true, double price = 0, int qtyNeeded = 0);
		Product(const Product&);
		Product& operator=(const Product&);
		~Product();
		std::fstream& store(std::fstream& file, bool newLine = true) const;
		std::fstream& load(std::fstream& file);
		std::ostream& write(std::ostream& os, bool linear) const;
		std::istream& read(std::istream& is);
		bool operator==(const char*) const;
		double total_cost() const;
		void quantity(int);
		bool isEmpty() const;
		int qtyNeeded() const;
		int quantity() const;
		bool operator>(const char*) const;
		bool operator>(const iProduct&) const;
		int operator+=(int);
	};
	std::ostream& operator<<(std::ostream&, const iProduct&);
	std::istream& operator>>(std::istream&, iProduct&);
	double operator+=(double&, const iProduct&);
}
#endif