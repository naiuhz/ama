/***********************************************************
// OOP244 Final Project Milestone 2
// ErrorState.cpp
// Date: 2018/03/22
// Author: Yiran Zhu
// Email: yzhu132@myseneca.ca
// Description: This milestone focuses on the ErrorState class
***********************************************************/

#include "ErrorState.h"
#include <iostream>

namespace AMA {
	ErrorState::ErrorState(const char* errorMessage) {
		if (errorMessage == nullptr) {
			storedMessage = nullptr;
		}
		else {
			message(errorMessage);
		}
	}

	ErrorState::~ErrorState() {
		delete[] storedMessage;
	}

	void ErrorState::clear() {
		storedMessage = nullptr;
	}

	bool ErrorState::isClear() const {
		if (storedMessage == nullptr) {
			return true;
		}
		return false;
	}

	void ErrorState::message(const char* str) {
		if (!isClear()) {
			delete[] storedMessage;
		}
		int strLength = (unsigned)strlen(str);
		storedMessage = new char[30];
		strncpy(storedMessage, str, strLength);
		storedMessage[strLength] = '\0';
	}

	const char* ErrorState::message() const {
		return storedMessage;
	}

	std::ostream& operator<<(std::ostream& os, const ErrorState& es) {
		if (!es.isClear()) {
			os << es.message();
		}
		return os;
	}
}