/***********************************************************
// OOP244 Final Project Milestone 1
// Date.h
// Date: 2018/03/16
// Author: Yiran Zhu
// Email: yzhu132@myseneca.ca
// Description: This milestone focuses on the Date class
***********************************************************/

#ifndef AMA_DATE_H
#define AMA_DATE_H
#include <iostream>

#define NO_ERROR 0   // No error - the date is valid
#define CIN_FAILED 1 // istream failed on information entry
#define YEAR_ERROR 2 // Year value is invalid
#define MON_ERROR 3  // Month value is invalid
#define DAY_ERROR 4  // Day value is invalid

namespace AMA {
	const int min_year = 2000;
	const int max_year = 2030;

	class Date {
		int year;
		int month;
		int day;
		int comparator;
		int error;
		int mdays(int, int) const;
		void errCode(int errorCode);
	public:
		Date();
		Date(int year, int month, int day);
		bool operator==(const Date& rhs) const;
		bool operator!=(const Date& rhs) const;
		bool operator<(const Date& rhs) const;
		bool operator>(const Date& rhs) const;
		bool operator<=(const Date& rhs) const;
		bool operator>=(const Date& rhs) const;
		int errCode() const;
		bool bad() const;
		std::istream& read(std::istream& istr);
		std::ostream& write(std::ostream& ostr) const;
	};

	std::ostream& operator<<(std::ostream& os, const Date& d);
	std::istream& operator>>(std::istream& is, Date& d);

}
#endif